const mymap = L.map("main_map").setView([6.2875927, -75.5510719], 15);
const access_Token =
  "pk.eyJ1IjoianRhMTIyMyIsImEiOiJja2VibHR6bGQwMGJvMnR0cGo2OWY4cDNpIn0.hoRWYfHPSM58Hbst8xd9fA";

L.tileLayer(
  `https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=${access_Token}`,
  {
    attribution:
      'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: "mapbox/streets-v11",
    tileSize: 512,
    zoomOffset: -1,
    accessToken: "your.mapbox.access.token",
  }
).addTo(mymap);

// var marker = L.marker([6.2875927, -75.5510719]).addTo(mymap);
// marker.bindPopup("<b>Cr 38 # 96 - 40</b><br>Medellín / Colombia").openPopup();

$.ajax({
  method: "POST",
  dataType: "json",
  url: "api/auth/authenticate",
  data: { email: "testing@testing.com", password: "test123" },
}).done(function (data) {
  $.ajax({
    dataType: "json",
    url: "api/bicicletas",
    beforeSend: (res) => {
      res.setRequestHeader("x-access-token", data.data.token);
    },
  }).done(function (result) {
    result.bicicletas.forEach((bici) => {
      L.marker(bici.ubicacion, { title: bici.id }).addTo(mymap);
    });
  });
});
