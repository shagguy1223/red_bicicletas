"use strict";

var mongoose = require("mongoose");

var request = require("request");

var Bicicleta = require("../../models/bicicleta");

var server = require("../../bin/www");

var base_url = "http://localhost:3000/api/bicicletas";
describe("Bicicleta API", function () {
  beforeAll(function (done) {
    mongoose.connection.close(done);
  });
  beforeEach(function () {
    var mongoDB = "mongodb://localhost:27017/testdb";
    mongoose.connect(mongoDB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true
    });
    var db = mongoose.connection;
    db.on("error", console.error.bind(console, "connection error"));
    db.once("open", function () {
      console.log("We are connected to test databese!");
    });
  });
  afterEach(function (done) {
    Bicicleta.deleteMany({}, function (err, success) {
      if (err) console.log(err);
      done();
    });
  });
  describe("GET BICICLETAS", function () {
    it("Status 200", function (done) {
      request.get(base_url, function (error, response, body) {
        var result = JSON.parse(body);
        expect(response.statusCode).toBe(200);
        expect(result.bicicletas.length).toBe(0);
        done();
      });
    });
  });
  describe("POST BICICLETAS /create", function () {
    it("STATUS 201", function (done) {
      var headers = {
        "content-type": "application/json"
      };
      var aBici = '{"code": 1, "color": "rojo", "modelo": "urbana", "lat": -34, "lng": -54}';
      request.post({
        headers: headers,
        url: "http://localhost:3000/api/bicicletas/create",
        body: aBici
      }, function (error, response, body) {
        expect(response.statusCode).toBe(201);
        Bicicleta.findByCode(1, function (error, targetBici) {
          expect(targetBici.code).toBe(1);
          expect(targetBici.color).toBe("rojo");
          expect(targetBici.modelo).toBe("urbana");
          done();
        });
      });
    });
  });
  describe(" UPDATE BICILECTAS /update", function () {
    it("actualizar una bicicleta", function (done) {
      var aBici = new Bicicleta({
        code: 1,
        color: "verde",
        modelo: "urbana"
      });
      aBici.save(function (err, bici) {
        if (err) console.log(err);
        Bicicleta.findOne({
          _id: bici._id
        }, "code color  modelo").exec(function (err, bicicleta) {
          if (err) console.log(err); // console.log(bicicleta);

          var headers = {
            "content-type": "application/json"
          };
          var abiciUpdate = '{ "code":1,"color":"Red","modelo":"Urbano","lat": "10.975832","lng": "-74.808815" }'; // console.log(bicicleta);

          request.post({
            headers: headers,
            url: "http://localhost:3000/api/bicicletas/update",
            body: abiciUpdate
          }, function (error, response, body) {
            expect(response.statusCode).toBe(404);
            Bicicleta.findByCode(1, function (err, bicicleta) {
              if (err) console.log(err); // console.log('after update => ' + bicicleta);

              expect(bicicleta.code).toBe(1);
              done();
            });
          });
        });
      }); // console.log('before update => ' + aBici);
    });
  });
  describe("POST BICICLETAS /DELETE", function () {
    it("STATUS 204", function (done) {
      var a = Bicicleta.createInstance(1, "rojo", "urbana", [-34.6012424, -58.3861497]);
      var b = Bicicleta.createInstance(2, "blanca", "urbana", [-34.596932, -58.3808287]);
      Bicicleta.add(a);
      Bicicleta.add(b);
      var headers = {
        "content-type": "application/json"
      };
      var aBici = '{"code": 1}';
      request["delete"]({
        headers: headers,
        url: "http://localhost:3000/api/bicicletas/delete",
        body: aBici
      }, function (error, response, body) {
        expect(response.statusCode).toBe(404);
        Bicicleta.allBicis(function (err, bicis) {
          expect(bicis.length).toBe(2);
        });
        done();
      });
    });
  });
});