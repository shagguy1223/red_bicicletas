const mongoose = require("mongoose");
const request = require("request");
const Bicicleta = require("../../models/bicicleta");
const server = require("../../bin/www");

const base_url = "http://localhost:3000/api/bicicletas";

describe("Bicicleta API", () => {
  beforeAll((done) => {
    mongoose.connection.close(done);
  });
  beforeEach(() => {
    const mongoDB = "mongodb://localhost:27017/testdb";
    mongoose.connect(mongoDB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    });
    const db = mongoose.connection;
    db.on("error", console.error.bind(console, "connection error"));
    db.once("open", () => {
      console.log("We are connected to test databese!");
    });
  });
  afterEach((done) => {
    Bicicleta.deleteMany({}, (err, success) => {
      if (err) console.log(err);
      done();
    });
  });

  describe("GET BICICLETAS", () => {
    it("Status 200", (done) => {
      request.get(base_url, function (error, response, body) {
        const result = JSON.parse(body);
        expect(response.statusCode).toBe(200);
        expect(result.bicicletas.length).toBe(0);
        done();
      });
    });
  });

  describe("POST BICICLETAS /create", () => {
    it("STATUS 201", (done) => {
      const headers = { "content-type": "application/json" };
      const aBici =
        '{"code": 1, "color": "rojo", "modelo": "urbana", "lat": -34, "lng": -54}';
      request.post(
        {
          headers: headers,
          url: "http://localhost:3000/api/bicicletas/create",
          body: aBici,
        },
        (error, response, body) => {
          expect(response.statusCode).toBe(201);
          const bici = JSON.parse(body);
          Bicicleta.findByCode({ _id: bici._id }, function (error, targetBici) {
            expect(targetBici.code).toBe(1);
            expect(targetBici.color).toBe("rojo");
            expect(targetBici.modelo).toBe("urbana");
            done();
          });
        }
      );
    });
  });

  describe(" UPDATE BICILECTAS /update", () => {
    it("actualizar una bicicleta", (done) => {
      const aBici = new Bicicleta({
        code: 2,
        color: "verde",
        modelo: "urbana",
      });
      aBici.ubicacion = [-54, -34];
      aBici.save((err, bici) => {
        if (err) console.log(err);
        Bicicleta.findByCode({ _id: bici._id }, (err, bicicleta) => {
          if (err) console.log(err);
          var headers = { "content-type": "application/json" };
          var abiciUpdate =
            '{ "code":1,"color":"Red","modelo":"Urbano","lat": 10.975832,"lng": -74.808815 }';
          request.patch(
            {
              headers: headers,
              url: `http://localhost:3000/api/bicicletas/update/${bicicleta._id}`,
              body: abiciUpdate,
            },
            function (error, response, body) {
              upBici = JSON.parse(body);
              expect(response.statusCode).toBe(200);
              expect(upBici.code).toBe(1);
              expect(upBici.color).toBe("Red");
              done();
            }
          );
        });
      });
    });
  });

  describe("POST BICICLETAS /DELETE", () => {
    it("STATUS 204", (done) => {
      var a = Bicicleta.createInstance(1, "rojo", "urbana", [
        -34.6012424,
        -58.3861497,
      ]);
      var b = Bicicleta.createInstance(2, "blanca", "urbana", [
        -34.596932,
        -58.3808287,
      ]);
      Bicicleta.add(a, (err, bici1) => {
        Bicicleta.add(b, (err, bici2) => {
          var headers = { "content-type": "application/json" };
          request.delete(
            {
              headers: headers,
              url: `http://localhost:3000/api/bicicletas/delete/${bici1._id}`,
              body: "",
            },
            function (error, response, body) {
              expect(response.statusCode).toBe(204);
              Bicicleta.allBicis(function (err, bicis) {
                expect(bicis.length).toBe(1);
              });
              done();
            }
          );
        });
      });
    });
  });
});
