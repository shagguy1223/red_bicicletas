"use strict";

var mongoose = require("mongoose");

var Bicicleta = require("../../models/bicicleta");

describe("Testing Bicicletas", function () {
  beforeAll(function (done) {
    mongoose.connection.close(done);
  });
  beforeEach(function () {
    var mongoDB = "mongodb://localhost:27017/testdb";
    mongoose.connect(mongoDB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true
    });
    var db = mongoose.connection;
    db.on("error", console.error.bind(console, "connection error"));
    db.once("open", function () {
      console.log("We are connected to test databese!");
    });
  });
  afterEach(function (done) {
    Bicicleta.deleteMany({}, function (err, success) {
      if (err) console.log(err);
      done();
    });
  });
  describe("Bicicleta.createInstance", function () {
    it("crea una instancia de Bicicleta", function () {
      var bici = Bicicleta.createInstance(1, "verde", "urbana", [-34.5, -54.1]);
      expect(bici.code).toBe(1);
      expect(bici.color).toBe("verde");
      expect(bici.modelo).toBe("urbana");
      expect(bici.ubicacion[0]).toBe(-34.5);
      expect(bici.ubicacion[1]).toBe(-54.1);
    });
  });
  describe("Bicicleta.allBicis", function () {
    it("comienza vacia", function (done) {
      Bicicleta.allBicis(function (err, bicis) {
        expect(bicis.length).toBe(0);
        done();
      });
    });
  });
  describe("Bicicleta.add", function () {
    it("agrega solo una bici", function (done) {
      var aBici = new Bicicleta({
        code: 1,
        color: "verde",
        modelo: "urbana"
      });
      Bicicleta.add(aBici, function (err, newBici) {
        if (err) console.log(err);
        Bicicleta.allBicis(function (err, bicis) {
          expect(bicis.length).toEqual(1);
          expect(bicis[0].code).toEqual(aBici.code);
          done();
        });
      });
    });
  });
  describe("Bicicleta.findByCode", function () {
    it("debe devolver la bici con code 1", function (done) {
      Bicicleta.allBicis(function (err, bicis) {
        expect(bicis.length).toBe(0);
        var aBici = new Bicicleta({
          code: 1,
          color: "verde",
          modelo: "urbana"
        });
        Bicicleta.add(aBici, function (err, newBici) {
          if (err) console.log(err);
          var aBici2 = new Bicicleta({
            code: 1,
            color: "verde",
            modelo: "urbana"
          });
          Bicicleta.add(aBici2, function (err, newBici) {
            if (err) console.log(err);
            Bicicleta.findByCode(1, function (error, targetBici) {
              expect(targetBici.code).toBe(aBici.code);
              expect(targetBici.color).toBe(aBici.color);
              expect(targetBici.modelo).toBe(aBici.modelo);
              done();
            });
          });
        });
      });
    });
  });
}); // beforeEach(() => {
//   Bicicleta.allBicis = [];
// });
// describe("Bicicleta.allBicis", () => {
//   it("comienza vacia", () => {
//     expect(Bicicleta.allBicis.length).toBe(0);
//   });
// });
// describe("Bicicleta.add", () => {
//   it("Agregamos una", () => {
//     expect(Bicicleta.allBicis.length).toBe(0);
//     const a = new Bicicleta(1, "rojo", "urbana", [6.2875927, -75.5510719]);
//     Bicicleta.add(a);
//     expect(Bicicleta.allBicis.length).toBe(1);
//     expect(Bicicleta.allBicis[0]).toBe(a);
//   });
// });
// describe("Bicicleta.findById", () => {
//   it("Debe devolver la bici con id 1", () => {
//     expect(Bicicleta.allBicis.length).toBe(0);
//     const aBici = new Bicicleta(2, "rojo", "urbana", [6.2875927, -75.5510719]);
//     const aBici2 = new Bicicleta(3, "verde", "montaña", [
//       6.2875927,
//       -75.5510719,
//     ]);
//     Bicicleta.add(aBici);
//     Bicicleta.add(aBici2);
//     const targetBici = Bicicleta.findById(2);
//     expect(targetBici.id).toBe(2);
//     expect(targetBici.color).toBe(aBici.color);
//     expect(targetBici.modelo).toBe(aBici.modelo);
//   });
// });
// describe("Bicicleas.removeById", () => {
//   it("Debe eliminar la bici con el id", () => {
//     expect(Bicicleta.allBicis.length).toBe(0);
//     const aBici = new Bicicleta(2, "rojo", "urbana", [6.2875927, -75.5510719]);
//     Bicicleta.add(aBici);
//     expect(Bicicleta.allBicis.length).toBe(1);
//     const targetBici = Bicicleta.findById(2);
//     for (let i = 0; i < Bicicleta.allBicis.length; i++) {
//       if (Bicicleta.allBicis[i].id == targetBici.id) {
//         Bicicleta.allBicis.splice(i, 1);
//         break;
//       }
//     }
//     expect(Bicicleta.allBicis.length).toBe(0);
//   });
// })