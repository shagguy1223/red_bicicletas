const passport = require("passport");
const LocalStrategy = require("passport-local").Strategy;
const FacebookStrategy = require("passport-facebook");
const FacebookTokenStrategy = require("passport-facebook-token");
const GoogleStrategy = require("passport-google-oauth20");
const Usuario = require("../models/usuario");

passport.use(
  new LocalStrategy(function (email, password, done) {
    Usuario.findOne({ email: email }, function (err, usuario) {
      if (err) return done(err);
      if (!usuario)
        return done(null, false, {
          message: "Email no existente o incorrecto.",
        });
      if (!usuario.validPassword(password))
        return done(null, false, { message: "Password incorrecto" });

      return done(null, usuario);
    });
  })
);

passport.use(
  new GoogleStrategy(
    {
      clientID: process.env.GOOGLE_CLIENT_ID,
      clientSecret: process.env.GOOGLE_CLIENT_SECRET,
      callbackURL: process.env.HOST + "/auth/google/callback",
    },
    function (accesToken, refreshToken, profile, cb) {
      Usuario.findOneOrCreateByGoogle(profile, function (err, user) {
        return cb(err, user);
      });
    }
  )
);

passport.use(
  new FacebookTokenStrategy(
    {
      clientID: process.env.FACEBOOK_ID,
      clientSecret: process.env.FACEBOOK_SECRET,
    },
    function (accesToken, refreshToken, profile, done) {
      try {
        Usuario.findOneOrCreateByFacebook(profile, function (err, user) {
          if (err) {
            console.log("Error" + err);
          }
          return done(err, user);
        });
      } catch (error) {
        console.log(error);
        return done(error, null);
      }
    }
  )
);

passport.use(
  new FacebookStrategy(
    {
      clientID: process.env.FACEBOOK_ID,
      clientSecret: process.env.FACEBOOK_SECRET,
      callbackURL: process.env.HOST +"/auth/facebook/callback",
      profileFields: ["id", "displayName", "email"],
    },
    function (accessToken, refreshToken, profile, cb) {
      console.log(profile)
      Usuario.findOneOrCreateByFacebook(profile, function (err, user) {
        if (err) {
          console.log(err)
        }
        return cb(err, user);
      });
    }
  )
);

passport.serializeUser(function (usuario, cb) {
  cb(null, usuario.id);
});

passport.deserializeUser(function (id, cb) {
  Usuario.findById(id, function (err, usuario) {
    cb(err, usuario);
  });
});

module.exports = passport;
