const nodemailer = require("nodemailer");
// const sgTransport = require("nodemailer-sendgrid-transport");

// let mailConfig;
// if (process.env.NODE_ENV === "production") {
//   const options = {
//     auth: {
//       api_key: process.env.SENDGRID_API_KEY,
//     },
//   };
//   mailConfig = sgTransport(options);
// } else {
//   if (process.env.NODE_ENV === "staging") {
//     console.log("XXXXXXXXXXXXXXXXX");
//     const options = {
//       auth: {
//         api_key: process.env.SENDGRID_API_KEY,
//       },
//     };
//     mailConfig = sgTransport(options);
//   } else {
//     mailConfig = {
//       host: "smtp.ethereal.email",
//       port: 587,
//       auth: {
//         user: process.env.ethereal_user,
//         pass: process.env.ethereal_pwd,
//       },
//     };
//   }
// }

let mailConfig;
if (process.env.NODE_ENV === "development") {
  mailConfig = {
    host: process.env.MAIL_HOST,
    port: 587,
    auth: {
      user: process.env.MAIL_USER,
      pass: process.env.MAIL_PWD,
    },
  };
} else {
  mailConfig = {
    host: process.env.MAIL_HOST,
    port: 587,
    auth: {
      user: process.env.MAIL_USER,
      pass: process.env.MAIL_PWD,
    },
  };
}

module.exports = nodemailer.createTransport(mailConfig);
