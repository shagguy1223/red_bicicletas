require("newrelic");
require("dotenv").config();
var createError = require("http-errors");
var express = require("express");
var path = require("path");
var cookieParser = require("cookie-parser");
var logger = require("morgan");
const passport = require("./config/passport");
const session = require("express-session");
const MongoDBStore = require("connect-mongodb-session")(session);
const { decode } = require("punycode");
const jwt = require("jsonwebtoken");

const indexRouter = require("./routes/index");
const bicicletasRouter = require("./routes/bicicletas");
const loginRouter = require("./routes/login");
const usersRouter = require("./routes/usuarios");
const tokenRouter = require("./routes/token");
const authAPIRouter = require("./routes/api/auth");
const bicicletasAPIRouter = require("./routes/api/bicicletas");
const reservasAPIRouter = require("./routes/api/reservas");
const usuariosAPIRouter = require("./routes/api/usuarios");

let store;
if (process.env.NODE_ENV === "development") {
  store = new session.MemoryStore();
} else {
  store = new MongoDBStore({
    uri: process.env.MONGO_URI,
    collection: "sessions",
  });
  store.on("error", function (err) {
    assert.ifError(err);
    assert.ok(false);
  });
}

var app = express();

app.set("secretKey", "jwt_pwd_!!223344");

app.use(
  session({
    cookie: { maxAge: 240 * 60 * 60 * 1000 },
    store: store,
    saveUninitialized: true,
    resave: "true",
    secret: 'red_bicis_!!!***!".!.!.123123',
  })
);

const mongoose = require("mongoose");
const { assert } = require("console");
const mongoDB = process.env.MONGO_URI;
mongoose.connect(mongoDB, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true,
});
mongoose.Promise = global.Promise;
const db = mongoose.connection;
db.on("error", console.error.bind(console, "MongoDB connection error"));

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "pug");

app.use(logger("dev"));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, "public")));

app.get(
  "/auth/google",
  passport.authenticate("google", {
    scope: [
      "https://www.googleapis.com/auth/plus.login",
      "https://www.googleapis.com/auth/plus.profile.emails.read",
      "profile",
      "email",
    ],
  })
);

app.get(
  "/auth/google/callback",
  passport.authenticate("google", {
    successRedirect: "/",
    failureRedirect: "/login",
  })
);

app.get(
  "/auth/facebook",
  passport.authenticate("facebook", {
    scope: ["email"],
  })
);

app.get(
  "/auth/facebook/callback",
  passport.authenticate("facebook", {
    failureRedirect: "/login",
  }),
  function (req, res) {
    res.redirect("/");
  }
);

app.use("/", indexRouter);
app.use("/usuarios", loggedIn, usersRouter);
app.use("/bicicletas", loggedIn, bicicletasRouter);
app.use("/", loginRouter);
app.use("/token", tokenRouter);
app.use("/api/auth", authAPIRouter);
app.use("/api/bicicletas", validarUsuario, bicicletasAPIRouter);
app.use("/api/usuarios", usuariosAPIRouter);
app.use("/api/reservas", validarUsuario, reservasAPIRouter);

app.use("/privacy_policy", function (req, res) {
  res.sendFile(path.join(__dirname, "./public/privacy_policy.html"));
});

app.use("/googlee47c2b2e26f62639", function (req, res) {
  res.sendFile(path.join(__dirname, "./public/googlee47c2b2e26f62639.html"));
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render("error");
});

function loggedIn(req, res, next) {
  if (req.user) {
    next();
  } else {
    console.log("Usuario sin loguearse");
    res.redirect("/login");
  }
}

function validarUsuario(req, res, next) {
  jwt.verify(req.headers["x-access-token"], req.app.get("secretKey"), function (
    err,
    decoded
  ) {
    if (err) {
      console.log("Error en validar usuario");
      res.json({ status: "error", message: err.message, data: null });
    } else {
      console.log("Pasó el usuario:" + req.body.userId);
      req.body.userId = decode.id;
      console.log("JWT verify:" + decoded);
      next();
    }
  });
}

module.exports = app;
