const Bicicleta = require("../models/bicicleta");

exports.bicicleta_list = (req, res) => {
  Bicicleta.allBicis((err, bicis) => {
    if (err) console.log(err);
    res.render("bicicletas/index", { bicis: bicis });
  });
};

exports.bicicleta_create_get = (req, res) => {
  res.render("bicicletas/create");
};

exports.bicicleta_create_post = (req, res) => {
  const bici = new Bicicleta({
    code: req.body.code,
    color: req.body.color,
    modelo: req.body.modelo,
  });
  bici.ubicacion = [req.body.lat, req.body.lng];
  bici.save(function (err) {
    if (err) console.log(err);
    res.redirect("/bicicletas");
  });
};

exports.bicicleta_update_get = (req, res) => {
  Bicicleta.findByCode({ _id: req.params.id }, (err, bici) => {
    res.render("bicicletas/update", { bici });
  });
};

exports.bicicleta_update_post = (req, res) => {
  let biciUpdate = {
    code: req.body.code,
    color: req.body.color,
    modelo: req.body.modelo,
  };
  biciUpdate.ubicacion = [req.body.lat, req.body.lng];
  Bicicleta.updateOne(biciUpdate, (err, result) => {
    if (err) console.log(err);
    res.redirect("/bicicletas");
  });
};

exports.bicicleta_delete_post = (req, res) => {
  Bicicleta.removeByCode({ _id: req.params.id }, (err, result) => {
    if (err) console.log(err);
    res.redirect("/bicicletas");
  });
};
