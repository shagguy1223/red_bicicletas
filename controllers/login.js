const passport = require("../config/passport");
const Usuario = require("../models/usuario");
const token = require("../models/token");

exports.login_get = (req, res) => {
  res.render("session/login");
};

exports.login_post = (req, res, next) => {
  passport.authenticate("local", (err, usuario, info) => {
    if (err) return next(err);
    if (!usuario) return res.render("session/login", { info });
    req.logIn(usuario, (err) => {
      console.log(usuario);
      if (err) return next(err);
      return res.redirect("/");
    });
  })(req, res, next);
};

exports.logout_get = (req, res) => {
  req.logout();
  res.redirect("/");
};

exports.forgotPassword_get = (req, res) => {
  res.render("session/forgotPassword");
};

exports.forgotPassword_post = (req, res, next) => {
  Usuario.findOne(
    {
      email: req.body.email,
    },
    (err, usuario) => {
      if (!usuario)
        return res.render("session/forgotPassword", {
          info: { message: "No existe el email para un usuario existente" },
        });
      usuario.resetPassword((err) => {
        if (err) return next(err);
        console.log("session/forgotPasswordMessage");
      });
      res.render("session/forgotPasswordMessage");
    }
  );
};

exports.resetPassword_get = (req, res, next) => {
  token.findOne({ token: req.params.token }, (err, token) => {
    if (!token)
      return res.status(400).send({
        msg:
          "No existe un usuario asociado al token, verifique que su token no haya expirado",
      });
    Usuario.findOne(token._userId, (err, usuario) => {
      if (!usuario)
        return res
          .status(400)
          .send({ msg: "No existe un usuario asociado al token" });
      res.render("session/resetPassword", { errors: {}, usuario: usuario });
    });
  });
};

exports.resetPassword_post = (req, res, next) => {
  if (req.body.password != req.body.confirm_password) {
    res.render("session/resetPassword", {
      errors: {
        confirm_password: { message: "No coincide con el password ingresado" },
      },
      usuario: new Usuario({ email: req.body.email }),
    });
    return;
  }
  Usuario.findOne({ email: req.body.email }, (err, usuario) => {
    usuario.password = req.body.password;
    usuario.save((err) => {
      if (err) {
        res.render("session/resetPassword", {
          errors: err.errors,
          usuario: new Usuario({ email: req.body.email }),
        });
      } else {
        res.redirect("/login");
      }
    });
  });
};
