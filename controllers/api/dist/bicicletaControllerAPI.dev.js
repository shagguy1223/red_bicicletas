"use strict";

var Bicicleta = require("../../models/bicicleta"); // exports.bicicleta_list = (req, res) => {
//   res.status(200).json({
//     bicicletas: Bicicleta.allBicis,
//   });
// };


exports.bicicleta_list = function (req, res) {
  Bicicleta.allBicis(function (err, bicis) {
    if (err) console.log(err);
    res.status(200).json({
      bicicletas: bicis
    });
  });
};

exports.bicicleta_create = function (req, res) {
  var bici = new Bicicleta({
    code: req.body.code,
    color: req.body.color,
    modelo: req.body.modelo
  });
  bici.ubicacion = [req.body.lat, req.body.lng];
  bici.save(function (err) {
    if (err) console.log(err);
    res.status(201).json(bici);
  });
};

exports.bicicleta_update = function (req, res) {
  console.log(req.body);
  Bicicleta.findByCode(req.body.code, function (err, aBici) {
    if (err) console.log(err);

    if (aBici === null) {
      res.status(500).json({
        message: "Id not found"
      });
    } else {
      var biciUpdate = {
        code: aBici.code,
        color: req.body.color,
        modelo: req.body.modelo
      };
      biciUpdate.ubicacion = [req.body.lat, req.body.lng];
      Bicicleta.updateOne(biciUpdate, function (err, result) {
        if (err) console.log(err);
        res.status(200).json(result);
      });
    }
  });
};

exports.bicicleta_delete = function (req, res) {
  Bicicleta.removeByCode(req.body.code, function (err, result) {
    if (err) console.log(err);
    res.status(204).send(result);
  });
};