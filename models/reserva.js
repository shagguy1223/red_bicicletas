const mongoose = require("mongoose");
const moment = require("moment");
const Schema = mongoose.Schema;

const reservaSchema = new Schema({
  desde: Date,
  hasta: Date,
  bicicleta: { type: mongoose.Schema.Types.ObjectId, ref: "Bicicletas" },
  usuario: { type: mongoose.Schema.Types.ObjectId, ref: "Usuario" },
});

reservaSchema.methods.diasDeReserva = function () {
  return moment(this.hasta).diff(moment(this.desde), "days") + 1;
};

reservaSchema.statics.allReservas = function (cb) {
  return this.find({}, cb);
};


module.exports = mongoose.model("Reserva", reservaSchema);
