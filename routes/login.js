const express = require("express");
const router = express.Router();
const loginController = require("../controllers/login");

router.get("/login", loginController.login_get);
router.post("/login", loginController.login_post);
router.get("/logout", loginController.logout_get);
router.get("/forgotPassword", loginController.forgotPassword_get);
router.post("/forgotPassword", loginController.forgotPassword_post);
router.get("/resetPassword/:token", loginController.resetPassword_get);
router.post("/resetPassword", loginController.resetPassword_post);

module.exports = router;
